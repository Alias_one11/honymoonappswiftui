import SwiftUI

struct SFSymbolComponent: View {
	// MARK: - ©PROPERTIES
	var sfSymbol: String
	var sizeFont: CGFloat = 24
	var fontWeight: Font.Weight = .regular
	
	// MARK: - ©Binding-->PROPERTIES
	@Binding var toView: Bool
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		///|__________|
		Button(action: {
			toView.toggle()
		}, label: {
			Image(systemName: sfSymbol)
				.font(.system(size: sizeFont, weight: fontWeight))
		}).accentColor(.primary)
		/*÷÷÷÷÷÷÷÷÷÷÷÷*/
		
	}///:END__>
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct SFSymbolComponent_Previews: PreviewProvider {
	@State static var generic: Bool = false
	@State static var generic2: Bool = false

	
	static var previews: some View {
		
		SFSymbolComponent(sfSymbol: "info.circle", toView: $generic)
			.preferredColorScheme(.dark)
	}
}
