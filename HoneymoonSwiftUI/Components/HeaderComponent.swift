import SwiftUI

struct HeaderComponent: View {
	// MARK: - ©PROPERTIES
	
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		VStack(alignment: .center, spacing: 20) {
			
			Capsule().frame(width: 120, height: 6)
				.foregroundColor(.secondary)
				.opacity(0.2)
			
			
			Image("logo-honeymoon").resizable().scaledToFit()
				.frame(height: 28)
		}
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct HeaderComponent_Previews: PreviewProvider {
	
	static var previews: some View {
		
		HeaderComponent().previewLayout(.sizeThatFits)
			.preferredColorScheme(.dark)
	}
}
