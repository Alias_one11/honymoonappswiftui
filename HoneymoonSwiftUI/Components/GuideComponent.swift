import SwiftUI

struct GuideComponent: View {
	// MARK: - ©PROPERTIES
	var title,
		subtitle,
		description,
		icon: String
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		HStack(alignment: .center, spacing: 20) {
			//|HEART-SFIMAGE|
			Image(systemName: icon).font(.largeTitle)
				.foregroundColor(.pink).padding(.leading, 4)
			
			VStack(alignment: .leading, spacing: 4) {
				
				HStack {
					//|TITLE-TEXT|
					Text(title.uppercased()).font(.title)
						.fontWeight(.heavy)
					Spacer()
					
					//|SUBTITLE-TEXT|
					Text(subtitle.uppercased()).font(.footnote)
						.fontWeight(.heavy).foregroundColor(.pink)
						.padding(.trailing)
				}///:END__>CHILD-HSTACK
				Divider().padding(.bottom, 4)
				
				//|DESCRIPTION-TEXT|
				Text(description).font(.footnote)
					.foregroundColor(.secondary)
					.fixedSize(horizontal: false, vertical: true)
				
			}///:END__>CHILD-VSTACK
		}//:END__>PARENT-VSTACK
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct GuideComponent_Previews: PreviewProvider {
	
	static var previews: some View {
		
		GuideComponent(title: "Title", subtitle: "Swipe right", description: "testing esto out to see how muc fits in this joint!, testing esto out to see how muc fits in this joint!", icon: "heart.circle")
			.previewLayout(.sizeThatFits)
			.preferredColorScheme(.dark)
	}
}
