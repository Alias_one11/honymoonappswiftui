import SwiftUI

struct ButtonModifier: ViewModifier {
	// MARK: - ©PROPERTIES
	var minframeWidth, maxframeWidth: CGFloat
	var txtFillColor: Color
	var fgColor: Color
	
	init(minframeWidth: CGFloat = 0,
		 maxframeWidth: CGFloat = .infinity,
		 txtFillColor: Color = .blue,
		 fgColor: Color = .white) {
		
		self.minframeWidth = minframeWidth
		self.maxframeWidth = maxframeWidth
		self.txtFillColor = txtFillColor
		self.fgColor = fgColor
	}
	
	/*©-----------------------------------------©*/
	func body(content: Content) -> some View {
		content.font(.headline).padding()
			.frame(minWidth: minframeWidth, maxWidth: maxframeWidth)
			.background(Capsule().fill(txtFillColor))
			.foregroundColor(fgColor)
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

