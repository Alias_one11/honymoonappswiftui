import SwiftUI

struct TitleModifier: ViewModifier {
	// MARK: - ©PROPERTIES
	var fontStyle: Font
	var fgColor: Color
	
	init(fontStyle: Font = .largeTitle, fgColor:Color = .pink) {
		self.fontStyle = fontStyle
		self.fgColor = fgColor
	}
	
	/*©-----------------------------------------©*/
	func body(content: Content) -> some View {
		content.font(fontStyle).foregroundColor(fgColor)
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

