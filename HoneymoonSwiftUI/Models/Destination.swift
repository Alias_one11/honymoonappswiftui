import SwiftUI

struct Destination: Identifiable {
	// MARK: - ©PROPERTIES
	var id = UUID().uuidString
	var place, country, image: String
}


