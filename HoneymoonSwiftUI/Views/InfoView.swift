import SwiftUI

struct InfoView: View {
	// MARK: - ©Environment
	@Environment(\.presentationMode) var presentationMode
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		ScrollView(.vertical, showsIndicators: false) {
			
			VStack(alignment: .center, spacing: 10) {
				// HEADERCOMPONENT()
				HeaderComponent()
				Spacer()
				
				Text("App Info").fontWeight(.black)
					.modifier(
						TitleModifier()
					)
				// APP INFO
				AppInfoSubView()
				
				
				Text("Credits").fontWeight(.black)
					.modifier(TitleModifier())
				// Credits
				CreditsSubView()
				Spacer(minLength: 10)
				
				Button(action: {
					// ACTION: Dismisses the view when the button is tapped
					presentationMode.wrappedValue.dismiss()
				}, label: {
					Text("Continue".uppercased())
						.modifier(
							ButtonModifier(txtFillColor: .pink)
						)
				})//=:END__>BUTTON
				
			}///:END__>SCROLLVIEW()
			.frame(minWidth: 0, maxWidth: .infinity)
			.padding(.vertical, 25).padding(.horizontal, 25)
			/*÷÷÷÷÷÷÷÷÷÷÷÷*/
			
		}//=:END__>VSTACK
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct InfoView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		InfoView().previewLayout(.sizeThatFits)
			.preferredColorScheme(.dark)
	}
}

// MARK: -#SUBVIEW
/*©-----------------------------------------©*/
struct AppInfoSubView: View {
	///|__________|
	var body: some View {
		///|__________|
		VStack(alignment: .leading, spacing: 10) {
			///|     App Info     |
			RowAppInfoSubView(item1: "Application", item2: "Honeymoon")
			RowAppInfoSubView(item1: "Compatibility", item2: "iPhone & iPad")
			RowAppInfoSubView(item1: "Developer", item2: "Alias111/ Sami Martin")
			RowAppInfoSubView(item1: "Designer", item2: "Alisia Huda")
			RowAppInfoSubView(item1: "Website", item2: "Alias111TheGreat@gmail.com")
			RowAppInfoSubView(item1: "Version", item2: "1.0.2")
		}
	}
}

struct RowAppInfoSubView: View {
	// MARK: - ©PROPERTIES
	var item1, item2: String
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		VStack {
			
			HStack {
				// Application
				Text(item1).foregroundColor(.gray)
				Spacer()// Moves to the leading edge
				
				// Honeymoon--> Spacer moves it to the trailing end
				Text(item2)
				
			}//=:END__>CHILD-HSTACK
			Divider()
		}
	}
	/*©-----------------------------------------©*/
}

struct CreditsSubView: View {
	// MARK: - ©PROPERTIES
	let credits: String = "Shifaaz Shamoon (Maldives), Grillot Edouard (France), Evan Wise (Greece), Christoph Schulz (United Arab Emirates), Andrew Coelho (USA), Damiano Baschiera (Italy), Daniel Olah (Hungary), Andrzej Rusinowski (Poland), Lucas Miguel (Slovenia), Florencia Potter (Spain), Ian Simmonds (USA), Ian Keefe (Canada), Denys Nevozhai (Thailand), David Köhler (Italy), Andre Benz (USA), Alexandre Chambon (South Korea), Roberto Nickson (Mexico), Ajit Paul Abraham (UK), Jeremy Bishop (USA), Davi Costa (Brazil), Liam Pozz (Australia)"
	
	var body: some View {
		
		VStack(alignment: .leading, spacing: 10) {
			HStack {
				// Photos
				Text("Photos").foregroundColor(.gray)
				Spacer()
				
				// Unsplash
				Text("Unsplash")
			}
			Divider()
			
			Text("Photographers").foregroundColor(.gray)
			
			// Photo credits
			Text(credits).multilineTextAlignment(.leading)
				.font(.footnote)
			
		}///:END__>VSTACK
	}
}
