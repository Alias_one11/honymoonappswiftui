import SwiftUI

// Helps with animation transition, only when the card is removed
extension AnyTransition {
	
	static var trailingBottom: AnyTransition {
		///|__________|
		AnyTransition.asymmetric(
			insertion: .identity,
			removal: AnyTransition.move(edge: .trailing).combined(with: .move(edge: .bottom))
		)
	}//:END__>
	
	static var leadingBottom: AnyTransition {
		///|__________|
		AnyTransition.asymmetric(
			insertion: .identity,
			removal: AnyTransition.move(edge: .leading).combined(with: .move(edge: .bottom))
		)
	}//:END__>
	
}///:END__>Extension
