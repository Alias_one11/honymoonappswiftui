import SwiftUI

struct GuideView: View {
	// MARK: - ©PROPERTIES
	let marketingHeadline: String = "Discover & pick the perfect" +
		" destination, for your romantic honeymoon!"
	
	let guideDesc: String = "Do you like the destination? Touch the screen & " +
		"swipe right. It will be saved to your favorites list."
	
	let dismissDesc: String = "Would you rather skip this location? " +
		"If YES, touch the screen & swipe left. You will no longer see this location"
	
	let bookDesc: String = "Our selections of honeymoon resorts are perfect " +
		"settings for you to embark on a beautiful getaway!"
	
	// MARK: - ©Environment-->PROPERTIES
	@Environment(\.presentationMode) var presentationMode
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		ScrollView(.vertical, showsIndicators: false) {
			
			VStack(alignment: .center, spacing: 15) {
				
				// HEADER-COMPONENT
				HeaderComponent()
				Spacer(minLength: 50)
				
				// GET STARTED-TEXT
				Text("Get Started").fontWeight(.black)
					.modifier(
						TitleModifier(fontStyle: .largeTitle, fgColor: .pink)
					)
				
				// MARKETING HEADELINE
				Text(marketingHeadline).lineLimit(nil)
					.multilineTextAlignment(.center)
				
				Spacer()
				
				// GUIDE-COMPONENT
				VStack(alignment: .leading, spacing: 25) {
					// LIKE SWIPE-RIGHT
					GuideComponent(title: "Like", subtitle: "Swipe Right",
								   description: guideDesc, icon: "heart.circle")
					// DISMISS SWIPE-LEFT
					GuideComponent(title: "Dismiss", subtitle: "Swipe Left",
								   description: dismissDesc, icon: "xmark.circle")
					// BOOK TAP THE-BUTTON
					GuideComponent(title: "Book", subtitle: "Tap The Button",
								   description: bookDesc, icon: "checkmark.square")
					
				}//=:END__>CHILD-VSTACK
				
				Spacer()
				
				Button(action: {
					// WILL DISMISS THE VIEW WHEN THE BUTTON IS TAPPED
					presentationMode.wrappedValue.dismiss()
				}, label: {
					Text("Continue".uppercased())
						.modifier(
							ButtonModifier(minframeWidth: 0, maxframeWidth: .infinity,
										   txtFillColor: .pink, fgColor: .white)
						)
				})//=:END__>BUTTON
				
			}///:END__>VSTACK
			.frame(minWidth: 0, maxWidth: .infinity)
			.padding(.top, 25).padding(.bottom, 25).padding(.horizontal, 25)
			/*÷÷÷÷÷÷÷÷÷÷÷÷*/
			
		}//=:END__>SCROLLVIEW
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct GuideView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		GuideView().previewLayout(.sizeThatFits)
			.preferredColorScheme(.dark)
	}
}
