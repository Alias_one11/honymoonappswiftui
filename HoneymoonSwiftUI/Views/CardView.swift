import SwiftUI

struct CardView: View, Identifiable {
	// MARK: - ©PROPERTIES
	var id = UUID().uuidString
	var honeymoon: Destination
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	fileprivate func honeyMoonText(place: String) -> some View {
		return
			Text(place)
			.foregroundColor(.white).font(.largeTitle)
			.fontWeight(.bold).shadow(radius: 1)
			.padding(.horizontal, 18).padding(.vertical, 4)
			// Over laying place text. Adds a divder line
			.overlay(
				Rectangle().fill(Color.white).frame(height: 1),
				// overlay second parameter
				alignment: .bottom
				///|__________|
			)//:END__>OVERLAY
	}///:END__>
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		Image(honeymoon.image).resizable().cornerRadius(24)
			.scaledToFit().frame(minWidth: 0, maxWidth: .infinity)
			
			// Over laying both text
			.overlay(
				VStack(alignment: .center, spacing: 12) {
					
					// PLACE
					honeyMoonText(place: honeymoon.place.uppercased())
					
					// COUNTRY
					textWithRoundedBackground(text: honeymoon.country.uppercased(),
											  fgColor: .black, frameMinWidth: 85)
					
				}///:END__>VSTACK
				.frame(minWidth: 280).padding(.bottom, 50),
				// overlay second parameter
				alignment: .bottom
			)//:END__>OVERLAY
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct CardView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CardView(honeymoon: HoneymoonData[0])
			.previewLayout(.sizeThatFits)
			.preferredColorScheme(.dark)
	}
}
