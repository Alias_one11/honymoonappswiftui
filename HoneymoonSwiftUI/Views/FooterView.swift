import SwiftUI

struct FooterView: View {
	// MARK: - ©Binding
	@Binding var showBookingAlert: Bool

	
	
	/**©---------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		HStack {
			// X-MARK Button
			sfSymbol(sfSymbol: "xmark.circle", sizeFont: 42, fontWeight: .light)
				.padding(.leading)
			
			Spacer()
			Button(action: {
				showBookingAlert.toggle()
			
			}, label: {
				Text("Book Destination".uppercased())
					.font(.system(.subheadline, design: .rounded))
					.fontWeight(.heavy).padding(.horizontal, 20)
					.padding(.vertical, 12).accentColor(.pink)
					.background(
						Capsule().stroke(Color.pink, lineWidth: 2)
					)
				
			})
			Spacer()

			// Heart Button
			sfSymbol(sfSymbol: "heart.circle", sizeFont: 42, fontWeight: .light)
				.padding(.trailing)
		}
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct FooterView_Previews: PreviewProvider {
	@State static var showAlert: Bool = false
	

	
	static var previews: some View {
		
		FooterView(showBookingAlert: $showAlert)
			.previewLayout(.sizeThatFits)
			.preferredColorScheme(.dark)
	}
}
