import SwiftUI

struct MainView: View {
	// MARK: -#typealias
	typealias t = AnyTransition
	
	// MARK: - ©PROPERTIES
	let alertMsg: String = "Wishing a lovely & precious time " +
		"together for the amazing couple!"
	
	// MARK: - ©State
	@State var showAlert: Bool = false
	@State var showGuide: Bool = false
	@State var showInfo: Bool = false
	@State private var lastCardIndex: Int = 1
	@State private var cardRemovalTransition = t.trailingBottom
	
	

	
	// MARK: - ©State-->COMPUTED-PROPERTY
	@State var cardViews: [CardView] = {
		///|     ⇪     |
		var views:[CardView]  = []
		
		///|Iterating through our honeymoon data|
		for index in 0..<2 {
			views.append(CardView(honeymoon: HoneymoonData[index]))
		}
		
		return views
	}()
	
	
	// MARK: - ©GestureState
	@GestureState private var dragState = DragState.inactive
	// Helper property over lays an x or heart, when the card is dragged
	private var dragAreaThreshold: CGFloat = 65.0
	
	
	
	/**©------------------------------------------------------------------------------©*/
	// MARK: - Helper methods
	
	fileprivate func isTopCard(card: CardView) -> Bool {
		///|__________|
		guard let index = cardViews.firstIndex(where: { $0.id == card.id }) else {
			return false
		}
		
		return index == 0
	}///:END__>Func
	
	// Removes a card
	fileprivate func moveCards() {
		///|__________|
		cardViews.removeFirst()
		lastCardIndex += 1// Increments by 1
		
		///|Since we are using a modules % symbol, the remainder
		///will always revert to the first card, if no card is present|
		let honeymoon = HoneymoonData[lastCardIndex % HoneymoonData.count]
		let newCardView = CardView(honeymoon: honeymoon)
		
		cardViews.append(newCardView)
	}
	/*©-----------------------------------------©*/
	var body: some View {
		
		VStack {
			// MARK: -HEADERVIEW-->TAP ON THE SF SYMBOL
			HeaderView(showGuideView: $showGuide, showInfoView: $showInfo)
				// Adds dragging capabilities
				.opacity(dragState.isDragging ? 0.0 : 1.0).animation(.default)
			
			Spacer()
			// MARK: - CARDVIEW
			ZStack {
				ForEach(cardViews) { card in
					card.zIndex(isTopCard(card: card) ? 1 : 0)
						/*÷÷÷÷÷÷÷÷÷÷÷÷*/
						.overlay(
							ZStack {
								// X-MARK SFSYMBOL
								Image(systemName: "x.circle")
									.modifier(SFSymbolModifier())
									///|By default the 'X' is hidden until the card is 						selected/tapped. When dragged to the left the 'X' will appear|
									.opacity(
										dragState.translation.width <
											-dragAreaThreshold && isTopCard(card: card) ? 1.0 : 0.0
									)
								/*÷÷÷÷÷÷÷÷÷÷÷÷*/
								
								// HEART-SFSYMBOL
								Image(systemName: "heart.circle")
									.modifier(SFSymbolModifier())
									///|By default the 'HEART' is hidden until the card is 					    						selected/tapped. When dragged to the right the 'HEART' will appear|
									.opacity(
										dragState.translation.width >
											dragAreaThreshold && isTopCard(card: card) ? 1.0 : 0.0
									)
								/*÷÷÷÷÷÷÷÷÷÷÷÷*/
							}///:END__>ZSTACK
						)//=:END__>.overlay
						/*÷÷÷÷÷÷÷÷÷÷÷÷*/
						.offset(
							x: isTopCard(card: card) ? dragState.translation.width : 0,
							y: isTopCard(card: card) ? dragState.translation.height : 0
						)
						// Changing the card size before the dragging animation
						.scaleEffect(dragState.isDragging && isTopCard(card: card) ? 0.85 : 1.0)
						// Rotates the card slightly before performing the full animation
						.rotationEffect(Angle(degrees: isTopCard(card: card) ?
												Double(dragState.translation.width / 12) : 0)
						)
						// Adding animation
						.animation(.interpolatingSpring(stiffness: 120, damping: 120))
						// LongPressGesture
						.gesture(
							LongPressGesture(minimumDuration: 0.01)
								.sequenced(before: DragGesture())
								.updating($dragState, body: { (value, state, transaction) in
									switch value {
										///|__________|
										case .first(true):
											state = .pressing
										case .second(true, let drag):
											state = .dragging(translation: drag?.translation ?? .zero)
										default: break
									}//=:END__>Switch
								})
								/*÷÷÷÷÷÷÷÷÷÷÷÷*/
								.onChanged({ (value) in
									// Updates change dependent on the users swipe direction
									guard case .second(true, let drag?) = value else { return }
									
									if drag.translation.width < dragAreaThreshold {
										cardRemovalTransition = .leadingBottom
									} else if drag.translation.width > dragAreaThreshold {
										cardRemovalTransition = .trailingBottom
									}
								})
								.onEnded({ (value) in
									guard case .second(true, let drag?) = value else { return }
									
									if drag.translation.width < -dragAreaThreshold ||
										drag.translation.width > dragAreaThreshold {
										moveCards()
									}
								})
						)//=:END__>.gesture-MODIFIER
						.transition(cardRemovalTransition)
					/*÷÷÷÷÷÷÷÷÷÷÷÷*/
					///|__________|
					/*÷÷÷÷÷÷÷÷÷÷÷÷*/
					
				}//=:END__>ForEach
			}///:END__>ZSTACK
			
			Spacer()
			
			// MARK: - FOOTERVIEW
			FooterView(showBookingAlert: $showAlert)
				.opacity(dragState.isDragging ? 0.0 : 1.0).animation(.default)
			
		}///:END__>VSTACK
		// ALERT AFTER THE HONEYMOON LOCATION BUTTON IS TAPPED
		.alert(isPresented: $showAlert) {
			Alert(title: Text("TRIP ADDED"), message: Text(alertMsg),
				  dismissButton: .default(Text("Enjoy Your Honeymoon!")))
		}//=:END__>Alert
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct MainView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		MainView().preferredColorScheme(.dark)
	}
}
