import SwiftUI

struct HeaderView: View {
	// MARK: - ©Binding
	@Binding var showGuideView: Bool
	@Binding var showInfoView: Bool
	
	/**©---------------------------------------------------------------©*/
	/*©-----------------------------------------©*/
	
	var body: some View {
		
		HStack {
			///|INFORMATION 􀅴 left|
			SFSymbolComponent(sfSymbol: "info.circle", toView: $showInfoView)
				// WILL SEGUE TO INFOVIEW()
				.sheet(isPresented: $showInfoView) {
					InfoView()
				}
			
			///|HEADER-TEXT-LOGO|
			Spacer()
			Image("logo-honeymoon-pink").resizable().scaledToFit()
				.frame(height: 28)
			Spacer()
			
			///|GUIDE 􀁜 right|
			SFSymbolComponent(sfSymbol: "questionmark.circle", toView: $showGuideView)
				// WILL SEGUE TO GUIDEVIEW()
				.sheet(isPresented: $showGuideView) {
					GuideView()
				}
			///|__________|
		}//:END__>HSTACK
		.padding()
	}
	
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct HeaderView_Previews: PreviewProvider {
	@State static var showGuide: Bool = false
	@State static var showInfo: Bool = false

	static var previews: some View {
		
		HeaderView(showGuideView: $showGuide, showInfoView: $showInfo)
			.previewLayout(.sizeThatFits)
			.preferredColorScheme(.dark)
	}
}
